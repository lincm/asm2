
# coding: utf-8

# ### COMP90024 Group 50 Analytics for report 
# 
# Author: Emanuela Arrigo 833250 

# In[254]:

import csv 
import pandas as pd 
import numpy as np
import os
import pickle

import scipy.stats as stats
import matplotlib.pyplot as plt
import seaborn as sns

import math
from scipy.stats import spearmanr
from scipy.stats import pearsonr


import sklearn as sk
from sklearn import svm
from sklearn import metrics
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
from sklearn.tree import DecisionTreeClassifier
from sklearn.linear_model import LogisticRegression


# ## AURIN DATA 

# In[410]:

#reading in AURIN DATA 

syd = pd.read_csv("syd_merged.csv") 
melb = pd.read_csv("melb_merged.csv") 
mel_syd = pd.read_csv("melb_sydmerged.csv") 


# In[411]:

melb['Rental_affordability'].fillna((melb['Rental_affordability'].mean()), inplace=True)
syd['Rental_affordability'].fillna((syd['Rental_affordability'].mean()), inplace=True)
mel_syd['Rental_affordability'].fillna((mel_syd['Rental_affordability'].mean()), inplace=True)
melb['Transport'].fillna((melb['Transport'].mean()), inplace=True)
mel_syd['Transport'].fillna((mel_syd['Transport'].mean()), inplace=True)

melb = melb.fillna(0)
syd = syd.fillna(0)
mel_syd = mel_syd.fillna(0)

melb['at_risk'] = melb['Rental_affordability'].apply(lambda x: 1 if x <= 100 else 0)
syd['at_risk'] = syd['Rental_affordability'].apply(lambda x: 1 if x <= 100 else 0)
mel_syd['at_risk'] = mel_syd['Rental_affordability'].apply(lambda x: 1 if x <= 100 else 0)


# #### Basic Statistics

# In[412]:

print('Average Melbourne Internet',np.average(melb['Internet']))
print('Average Melbourne Tranposrt', np.average(melb['Transport']))
print('Average Melbounre Rental', np.average(melb['Rental_affordability']))

print('Average Sydney Internet', np.average(syd['Internet']))
print('Average Sydney Tranport', np.average(syd['Transport']))
print('Average Sydney Rental', np.average(syd['Rental_affordability']))


# #### Statistical Tests 

# In[405]:

#Only statistically significant difference between the two cities 

stats.ttest_ind(a=melb['Rental_affordability'],b=syd['Rental_affordability'],equal_var=False)


# In[406]:

stats.ttest_ind(a=melb['Internet'],b=syd['Internet'],equal_var=False)


# In[407]:

stats.ttest_ind(a=melb['Transport'],b=syd['Transport'],equal_var=False)


# ### Classification of Melbourne or Sydney: 

# In[413]:

feature_names = ['Transport', 'Internet', 'Rental_affordability',
       'Household_income', 'Median_age', 'Median_mortgage', 'Median_rent',
       'Family_income', 'Median_housesize', 'Median_personal_income']
X_both = mel_syd[feature_names]
y_both = mel_syd['Syd_Melb']

X_trainb, X_testb, y_trainb, y_testb = train_test_split(X_both, y_both, random_state=0)
scaler = MinMaxScaler()
X_trainb = scaler.fit_transform(X_trainb)
X_testb = scaler.transform(X_testb)


# In[414]:

clf = DecisionTreeClassifier().fit(X_trainb, y_trainb)
print('Accuracy of Decision Tree classifier on training set: {:.2f}'
     .format(clf.score(X_trainb, y_trainb)))
print('Accuracy of Decision Tree classifier on test set: {:.2f}'
     .format(clf.score(X_testb, y_testb)))


# In[415]:

SVM = svm.LinearSVC()
SVM.fit(X_trainb, y_trainb)

print('Accuracy of SVM on training set: {:.2f}'
     .format(SVM.score(X_trainb, y_trainb)))
print('Accuracy of SVM on training set: {:.2f}'
     .format(SVM.score(X_testb, y_testb)))

predictions = SVM.predict(X_testb)


# In[416]:

logreg = LogisticRegression()
logreg.fit(X_trainb, y_trainb)
print('Accuracy of Logistic regression classifier on training set: {:.2f}'
     .format(logreg.score(X_trainb, y_trainb)))
print('Accuracy of Logistic regression classifier on test set: {:.2f}'
     .format(logreg.score(X_testb, y_testb)))


# Confusion matrix for this classification

# In[286]:

predictions = logreg.predict(X_testb)
cm = metrics.confusion_matrix(y_testb, predictions)
print(cm)


sns.heatmap(cm, annot=True, fmt=".3f", linewidths=.5, square = True, cmap = 'Blues_r');
plt.ylabel('Actual label');
plt.xlabel('Predicted label');
plt.show()


# ### Classification of At Risk 

# In[417]:

feature_names = ['Transport', 'Internet', 'Rental_affordability',
       'Household_income', 'Median_age', 'Median_mortgage', 'Median_rent',
       'Family_income', 'Median_housesize', 'Median_personal_income']
X_bothr = mel_syd[feature_names]
y_bothr = mel_syd['at_risk']

X_trainbr, X_testbr, y_trainbr, y_testbr = train_test_split(X_bothr, y_bothr, random_state=0)
scaler = MinMaxScaler()
X_trainbr = scaler.fit_transform(X_trainbr)
X_testbr = scaler.transform(X_testbr)


# In[418]:

from sklearn.tree import DecisionTreeClassifier
clf = DecisionTreeClassifier().fit(X_trainbr, y_trainbr)
print('Accuracy of Decision Tree classifier on training set: {:.2f}'
     .format(clf.score(X_trainbr, y_trainbr)))
print('Accuracy of Decision Tree classifier on test set: {:.2f}'
     .format(clf.score(X_testbr, y_testbr)))


# In[419]:

from sklearn.linear_model import LogisticRegression
logreg = LogisticRegression()
logreg.fit(X_trainbr, y_trainbr)
print('Accuracy of Logistic regression classifier on training set: {:.2f}'
     .format(logreg.score(X_trainbr, y_trainbr)))
print('Accuracy of Logistic regression classifier on test set: {:.2f}'
     .format(logreg.score(X_testbr, y_testbr)))


# ### Correlation Matrices

# In[428]:

corr_melbourne = melb[['Transport', 'Internet', 'Rental_affordability',
       'Household_income', 'Median_age', 'Median_mortgage', 'Median_rent',
       'Family_income', 'Median_housesize', 'Median_personal_income']].corr()
ax = sns.heatmap(
    corr_melbourne, 
    square=True
)
ax.set_xticklabels(
    ax.get_xticklabels(),
    rotation=45,
    horizontalalignment='right'
)

plt.show()


# In[ ]:




# In[427]:

corr_syd = syd[['Transport', 'Internet', 'Rental_affordability',
       'Household_income', 'Median_age', 'Median_mortgage', 'Median_rent',
       'Family_income', 'Median_housesize', 'Median_personal_income']].corr()
ax = sns.heatmap(
    corr_syd, 
    square=True
)
ax.set_xticklabels(
    ax.get_xticklabels(),
    rotation=45,
    horizontalalignment='right'
)

plt.show()


# In[426]:

corr_both = mel_syd[['Transport', 'Internet', 'Rental_affordability',
       'Household_income', 'Median_age', 'Median_mortgage', 'Median_rent',
       'Family_income', 'Median_housesize', 'Median_personal_income']].corr()
ax = sns.heatmap(
    corr_both, 
    square=True
)
ax.set_xticklabels(
    ax.get_xticklabels(),
    rotation=45,
    horizontalalignment='right'
)

plt.show()


# In[420]:

corr_just3 = mel_syd[['Transport', 'Internet', 'Rental_affordability']].corr()
ax = sns.heatmap(
    corr_just3, 
    square=True
)
ax.set_xticklabels(
    ax.get_xticklabels(),
    rotation=45,
    horizontalalignment='right'
)

plt.show()


# In[432]:

corr, _ = pearsonr(mel_syd['Rental_affordability'], mel_syd['Transport'])
print(corr)


# In[375]:

from pandas.plotting import scatter_matrix
scatter_matrix(X_both, alpha=0.2, figsize=(6, 6), diagonal='kde')
plt.show()



# In[433]:

mel_syd.groupby('Syd_Melb').hist()
plt.show()


# In[388]:

mel_syd.groupby('Syd_Melb').Rental_affordability.hist(alpha=0.4)
plt.title('Histogram of Rental Affordability by Location')
plt.legend(('Melbourne', 'Sydney'))
plt.show()


# ### Visualisation 

# In[421]:

import matplotlib.pyplot as plt
mel_syd[['Internet', 'Transport', 'Rental_affordability']].boxplot()
plt.title('Boxplot of Melbourne key attributes')
plt.show()


# In[385]:

mel_syd[['Internet']].boxplot()
plt.show()


# In[422]:

syd[['Internet', 'Transport', 'Rental_affordability']].boxplot()
plt.title('Boxplot of Sydney key attributes')
plt.show()


# In[425]:

from pandas.plotting import scatter_matrix
scatter_matrix(mel_syd[['Internet', 'Transport', 'Rental_affordability']], alpha=0.2, figsize=(6, 6), diagonal='kde')
plt.show()


# In[322]:




# In[331]:




# In[ ]:



