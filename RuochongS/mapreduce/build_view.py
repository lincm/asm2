import couchdb
import json
import sys

def build_view(view, dbname, file=None, server='http://user:123@127.0.0.1:5984/'):
    try:
        couch_server = couchdb.Server(server)
        db = couch_server[dbname]
        if file:
            view = json.load(open(file))
        db.save(view)
        print('OK')
    except e:
        print(e)
        
if __name__ == '__main__':
    build_view(None, sys.argv[1], file=sys.argv[2], server=sys.argv[3])
