# README

## 1. aurin

Includes

* Json data files from AURIN 
* A handler merging informations from AURIN and tweets data results from views of CouchDB

## 2. mapreduce

Includes

* Views of CouchDB built for analysis
* Analysis in MapReduceAnalysis.ipynb
* Python script for building views on CouchDB

## 3. reactionrnn

Reference: https://github.com/minimaxir/reactionrnn

Provides a set of offline solution of sentiment analysis for tweets. Can give scores on local files.

