# COMP90024-ASSIGNMENT2-GROUP-50

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

This is the solution provided by group 50 (**M. Pan**, **E. Arrigo**, **C. Lin**, **R. Shen** and **X. Wang**) regarding to the project provided by **Prof. Richard Sinnott** (rsinnott@unimelb.edu.au).

This repository contains files including:

* Ansible Solution
* Web App
* Twitter Harvester
* Aurin data sample
* MapReduce & anaysis

# Architecture of the System
![System Architecture](https://drive.google.com/uc?export=view&id=1kKRM86yU5boJmQF_Ty5RcdGBxnNaLe-l)
# Installation
```sh
$ git clone git@bitbucket.org:lincm/asm2.git
```
# Ansible
The system can be deployed with one key.

First, go to the ansible directory
```sh
$ cd asm2/ansible\ script/
```
Change the configuration including 

* server variables, e.g. number of instances, volumes size per instance etc. (/hosts_vars/nectar.yaml)
* username & password for couchdb (/hosts_vars/docker.yaml)
* replace open.rc with your own
* ssh key for server (hosts)

You are ready to go!
```sh
$ ./run-all-in-one.sh
```

If you want to add extra server nodes to the system, make sure you modify the hosts file.
You should include your current server ip addresses, and modify the nectar variable file to specify the number of extra instances and its configuration. Next, add the configuration file for the harvester, then run the command
```sh
$ ./add-dbnode.sh
```
It can automatically create new instances and join the cluster node as well as the harvester.

# Twitter Harvester
Twitter harvester has 2 modes available:

* stream mode: get real time tweets
* timelines: mining user historical tweets

You can run the twitter harvester locally.
Go to the relative directory:
```sh
$ cd asm2/xuanzhe
```
Change your configuration file which contains:

* Twitter app credential
* database name for tweets
* database name for user
* place bounding box
* CouchDB username & password

Then twitter harvester can be runned by:
```sh
$ ./python harvester.py -c <your-config-file> -m <stream/timelines> -s <your-couchdb-server-ip>
```
Notice: if you are going to run it locally, please do check the dependency list below, make sure you have all of them installed.

# Web Hosting
The web application is based on Django 3, for a list of all the requirements, please refer to [requirements.txt](https://bitbucket.org/lincm/asm2/src/master/web/requirements.txt) under web folder. Redis also needs to be installed for pre-processing.

To run at local machine, using *virtualenv* is recommended. Please *NOT* use the venv folder and create a new virtual environment instead.
```sh
$ pip install -r asm2/web/requirements.txt
$ sudo apt-get install redis-server
```

Once the environment is set up and the requirements are installed, go to the webapp directory and start the application

```sh
$ cd asm2/web/webapp
$ python manage.py runserver
```

# MapReduce
Please refer to readme.md under RuochongS/. 

# Dependencies
* #### Ansible
* #### Python3
* #### Django
* #### Numpy
* #### Couchdb
* #### Tweepy
* #### Git

# Copyright
This repo is for the demostration purposed, Copy right reserved to ©**Prof. richard Sinnott** and **each member of Group 50**. 