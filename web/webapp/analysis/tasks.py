from __future__ import absolute_import, unicode_literals
from django.core.cache import cache

import collections
from couchdb import Server
from heapq import nlargest
from operator import itemgetter
from celery import shared_task

def connect_server():
    server = None
    with open('couch_docs/_couchdb_server_ip.config') as f:
        line = f.readline().rstrip()
        if line != 'http://admin:admin@localhost:5984/':
            server = Server(line)
    
    return server

@shared_task
def process():
    server = connect_server()
    melb_count = []
    syd_count = []
    melb_sentimentPosNeg = []
    syd_sentimentPosNeg = []
    melb_sentimentTenth = []
    syd_sentimentTenth = []
    melb_sentimentTenth_Pos, melb_sentimentTenth_Neg, syd_sentimentTenth_Pos, syd_sentimentTenth_Neg = [], [], [], []

    if server is not None:
        view_results = server['melb_tweets'].view('analysis/countHashtag', group=True, reduce=True)
        for data in view_results:
            melb_count.append([data.key, data.value])
        melb_count = nlargest(5, melb_count, key=itemgetter(1))
        
        view_results = server['syd_tweets'].view('analysis/countHashtag', group=True, reduce=True)
        for data in view_results:
            syd_count.append([data.key, data.value])
        syd_count = nlargest(5, syd_count, key=itemgetter(1))

        melb_sentimentPosNeg = []
        view_results = server['melb_tweets'].view('analysis/sentimentPosNeg', group=True, reduce=True)
        for data in view_results:
            melb_sentimentPosNeg.append([data.key, data.value])

        syd_sentimentPosNeg = []
        view_results = server['syd_tweets'].view('analysis/sentimentPosNeg', group=True, reduce=True)
        for data in view_results:
            syd_sentimentPosNeg.append([data.key, data.value])

        melb_sentimentTenth = []
        view_results = server['melb_tweets'].view('analysis/sentimentTenth', group=True, reduce=True)
        for data in view_results:
            melb_sentimentTenth.append([data.key, data.value])

        syd_sentimentTenth = []
        view_results = server['syd_tweets'].view('analysis/sentimentTenth', group=True, reduce=True)
        for data in view_results:
            syd_sentimentTenth.append([data.key, data.value])

        melb_sentimentTenth_Pos = melb_sentimentTenth[10:]
        melb_sentimentTenth_Neg = melb_sentimentTenth[:11][::-1]
        syd_sentimentTenth_Pos = syd_sentimentTenth[10:]
        syd_sentimentTenth_Neg = syd_sentimentTenth[:11][::-1]
    
    cache.set('melb_count', melb_count)
    cache.set('syd_count', syd_count)
    cache.set('melb_sentimentPosNeg', melb_sentimentPosNeg)
    cache.set('syd_sentimentPosNeg', syd_sentimentPosNeg)
    cache.set('melb_sentimentTenth_Pos', melb_sentimentTenth_Pos)
    cache.set('syd_sentimentTenth_Pos', syd_sentimentTenth_Pos)
    cache.set('melb_sentimentTenth_Neg', melb_sentimentTenth_Neg)
    cache.set('syd_sentimentTenth_Neg', syd_sentimentTenth_Neg)

    keyList = ['melb_count', 'syd_count', 'melb_sentimentPosNeg', 'syd_sentimentPosNeg', 'melb_sentimentTenth_Pos', 'syd_sentimentTenth_Pos', 'melb_sentimentTenth_Neg', 'syd_sentimentTenth_Neg',]
    valueList = [melb_count, syd_count, melb_sentimentPosNeg, syd_sentimentPosNeg, melb_sentimentTenth_Pos, syd_sentimentTenth_Pos, melb_sentimentTenth_Neg, syd_sentimentTenth_Neg,]
    results = dict(zip(keyList, valueList))

    return results
