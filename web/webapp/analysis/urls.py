from django.urls import path

from . import views

app_name = 'analysis'
urlpatterns = [
    path('', views.index, name='index'),
    path('dashboard/', views.dashboard, name='dashboard'),
    path('transport/', views.transport, name='transport'),
    path('rental/', views.rental, name='rental'),
    path('internet/', views.internet, name='internet'),
    path('map/', views.map, name='map'),
]
