from django.shortcuts import render
from django.shortcuts import redirect
from django.urls import reverse
from django.contrib.staticfiles.storage import staticfiles_storage
from django.core.cache import cache

import collections
from couchdb import Server
from heapq import nlargest
from operator import itemgetter

from analysis.tasks import connect_server, process

# Create your views here.
server = connect_server()

def index(request):
    return redirect(reverse('analysis:dashboard'))

def dashboard(request):
    results = None
    melb_count = cache.get('melb_count')
    syd_count = cache.get('syd_count')
    melb_sentimentPosNeg = cache.get('melb_sentimentPosNeg')
    syd_sentimentPosNeg = cache.get('syd_sentimentPosNeg')
    melb_sentimentTenth_Pos = cache.get('melb_sentimentTenth_Pos')
    melb_sentimentTenth_Neg = cache.get('melb_sentimentTenth_Neg')
    syd_sentimentTenth_Pos = cache.get('syd_sentimentTenth_Pos')
    syd_sentimentTenth_Neg = cache.get('syd_sentimentTenth_Neg')

    if syd_sentimentTenth_Neg is None:
        results = process()
    else:
        keyList = ['melb_count', 'syd_count', 'melb_sentimentPosNeg', 'syd_sentimentPosNeg', 'melb_sentimentTenth_Pos', 'syd_sentimentTenth_Pos', 'melb_sentimentTenth_Neg', 'syd_sentimentTenth_Neg',]
        valueList = [melb_count, syd_count, melb_sentimentPosNeg, syd_sentimentPosNeg, melb_sentimentTenth_Pos, syd_sentimentTenth_Pos, melb_sentimentTenth_Neg, syd_sentimentTenth_Neg,]
        results = dict(zip(keyList, valueList))

    return render(request, 'analysis/dashboard.html', results)

def transport(request):
    melb_total = 0
    syd_total = 0
    melb_ctr = 0
    syd_ctr = 0
    melb_heat = []
    syd_heat = []

    melb_sum = 0
    syd_sum = 0
    melb_count = 1
    syd_count = 1
    melb_sumsqr = 0
    syd_sumsqr = 0

    with open(staticfiles_storage.path('data/melb_transport.csv')) as f:
        f.readline()
        melb_min = 100
        melb_max = 0
        for line in f:
            data = line.split(',')
            accessibility = float(data[1])
            melb_total += accessibility
            melb_ctr += 1
            if len(data[2]) == 0 or len(data[3]) == 0:
                continue
            if melb_min > accessibility:
                melb_min = accessibility
            if melb_max < accessibility:
                melb_max = accessibility
            melb_heat.append([float(data[1]), data[2], data[3]])
        
        for row in melb_heat:
            row[0] = 100 +((row[0])-melb_min)/(melb_max-melb_min)
    
    with open(staticfiles_storage.path('data/syd_transport.csv')) as f:
        f.readline()
        syd_min = 100
        syd_max = 0
        for line in f:
            data = line.split(',')
            accessibility = float(data[1])
            syd_total += accessibility
            syd_ctr += 1
            if len(data[2]) == 0 or len(data[3]) == 0:
                continue
            if syd_min > accessibility:
                syd_min = accessibility
            if syd_max < accessibility:
                syd_max = accessibility
            syd_heat.append([float(data[1]), data[2], data[3]])
        
        for row in syd_heat:
            row[0] = 100 + ((row[0])-syd_min)/(syd_max-syd_min)
    
    melb_avg = melb_total / melb_ctr
    syd_avg = syd_total / syd_ctr

    if server is not None:
        view_results = server['melb_tweets'].view('topicAna/transport', group=True, reduce=True)
        for data in view_results:
            melb_sum = data.value.get('sum')
            melb_count = data.value.get('count')
            melb_sumsqr = round(data.value.get('sumsqr'), 2)

        view_results = server['syd_tweets'].view('topicAna/transport', group=True, reduce=True)
        for data in view_results:
            syd_sum = data.value.get('sum')
            syd_count = data.value.get('count')
            syd_sumsqr = round(data.value.get('sumsqr'), 2)

    melb_sentiment = round(melb_sum / melb_count, 4)
    syd_sentiment = round(syd_sum / syd_count, 4)
    
    return render(request, 'analysis/transport.html', {
        'melb_avg': melb_avg, 'melb_heat': melb_heat,
        'syd_avg': syd_avg, 'syd_heat': syd_heat,
        'melb_sentiment': melb_sentiment, 'melb_sumsqr': melb_sumsqr,
        'syd_sentiment': syd_sentiment, 'syd_sumsqr': syd_sumsqr
    })

def rental(request):
    melb_total = 0
    syd_total = 0
    melb_ctr = 0
    syd_ctr = 0
    melb_cities = []
    syd_cities = []

    melb_sum = 0
    syd_sum = 0
    melb_count = 1
    syd_count = 1
    melb_sumsqr = 0
    syd_sumsqr = 0

    with open(staticfiles_storage.path('data/melb_rental.csv')) as f:
        f.readline()
        for line in f:
            data = line.split(',')
            if data[2] != 'null':
                melb_total += float(data[2])
                melb_ctr += 1
            if len(data[3]) > 0 and len(data[4]) > 0:
                melb_cities.append([data[2],data[3],data[4]])
    
    with open(staticfiles_storage.path('data/syd_rental.csv')) as f:
        f.readline()
        for line in f:
            data = line.split(',')
            if data[2] != 'null':
                syd_total += float(data[2])
                syd_ctr += 1
            if len(data[3]) > 0 and len(data[4]) > 0:
                syd_cities.append([data[2],data[3],data[4]])

    melb_avg = melb_total / melb_ctr
    syd_avg = syd_total / syd_ctr

    if server is not None:
        view_results = server['melb_tweets'].view('topicAna/rent', group=True, reduce=True)
        for data in view_results:
            melb_sum = data.value.get('sum')
            melb_count = data.value.get('count')
            melb_sumsqr = round(data.value.get('sumsqr'), 2)

        view_results = server['syd_tweets'].view('topicAna/rent', group=True, reduce=True)
        for data in view_results:
            syd_sum = data.value.get('sum')
            syd_count = data.value.get('count')
            syd_sumsqr = round(data.value.get('sumsqr'), 2)

    melb_sentiment = round(melb_sum / melb_count, 4)
    syd_sentiment = round(syd_sum / syd_count, 4)
    
    return render(request, 'analysis/rental.html', {
        'melb_avg': melb_avg, 'melb_cities': melb_cities,
        'syd_avg': syd_avg, 'syd_cities': syd_cities,
        'melb_sentiment': melb_sentiment, 'melb_sumsqr': melb_sumsqr,
        'syd_sentiment': syd_sentiment, 'syd_sumsqr': syd_sumsqr
    })

def internet(request):
    melb_total = 0
    melb_ctr = 0
    syd_total = 0
    syd_ctr = 0

    melb_sum = 0
    syd_sum = 0
    melb_count = 1
    syd_count = 1
    melb_sumsqr = 0
    syd_sumsqr = 0    

    with open(staticfiles_storage.path('data/melb_internet.csv')) as f:
        f.readline()
        for line in f:
            melb_total += float(line.split(',')[1])
            melb_ctr += 1

    with open(staticfiles_storage.path('data/syd_internet.csv')) as f:
        f.readline()
        for line in f:
            syd_total += float(line.split(',')[1])
            syd_ctr += 1

    melb_covered = round(melb_total / melb_ctr, 2)
    melb_not_covered = round(100 - melb_covered, 2)
    syd_covered = round(syd_total / syd_ctr, 2)
    syd_not_covered = round(100 - syd_covered, 2)

    if server is not None:
        view_results = server['melb_tweets'].view('topicAna/internet', group=True, reduce=True)
        for data in view_results:
            melb_sum = data.value.get('sum')
            melb_count = data.value.get('count')
            melb_sumsqr = round(data.value.get('sumsqr'), 2)

        view_results = server['syd_tweets'].view('topicAna/internet', group=True, reduce=True)
        for data in view_results:
            syd_sum = data.value.get('sum')
            syd_count = data.value.get('count')
            syd_sumsqr = round(data.value.get('sumsqr'), 2)

    melb_sentiment = round(melb_sum / melb_count, 4)
    syd_sentiment = round(syd_sum / syd_count, 4)

    return render(request, 'analysis/internet.html', {
        'melb_covered': melb_covered, 'melb_not_covered': melb_not_covered,
        'syd_covered': syd_covered, 'syd_not_covered': syd_not_covered,
        'melb_sentiment': melb_sentiment, 'melb_sumsqr': melb_sumsqr,
        'syd_sentiment': syd_sentiment, 'syd_sumsqr': syd_sumsqr
    })

def map(request):
    return render(request, 'analysis/map.html')
