from django.shortcuts import render
from analysis.tasks import process

# Create your views here.
def index(request):
    process.delay()
    return render(request, 'home/index.html')