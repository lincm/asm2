import couchdb
import json
import requests

# def build_view(view, dbname, server='http://ccc-grp-50:1035403@172.26.129.216:5984'):
#     couch_server = couchdb.Server(server)
#     db = couch_server[dbname]
#     db.save(view)

def list_results(links):
    ret = []
    for link in links:
        response = requests.get(link)
        try:
            res = json.loads(response.text)
        except:
            print(response)
            continue
        
        try:
            ret.append((link, res['rows']))
        except:
            print(res)
            continue
    return ret

server_link = 'http://ccc-grp-50:1035403@172.26.129.216:5984/'
melb_view_link = 'melb_tweets/_design/newView/_view/'
countCity_link = 'countCity?reduce=true&group=true'
countHashtag_link = 'countHashtag?reduce=true&group=true'
hourofDay_link = 'hourofDay'
sentimentTenth_link = 'sentimentTenth?reduce=true&group=true'
sentimentPosNeg_link = 'sentimentPosNeg?reduce=true&group=true'

# view_sample = 
# {
#   "_id": "_design/newView",
#   "_rev": "9-f9e01d4f3c69e346ef2d4c7258b9d823",
#   "views": {
#     "countHashtag": {
#       "reduce": "_count",
#       "map": "function (doc) {\n    if (doc.tweet) {\n        if (doc.tweet.entities && doc.tweet.entities.hashtags) {\n            doc.tweet.entities.hashtags.forEach(function (hashtag) {\n                if (hashtag.text) {\n                    emit(hashtag.text.toLowerCase(), 1);\n                }\n            });\n        }\n    }\n}"
#     },
#     "countCity": {
#       "reduce": "_count",
#       "map": "function (doc) {\n    if (doc.tweet) {\n        var dtw;\n        dtw = doc.tweet;\n        if ((dtw.place && dtw.place.name.toLowerCase() === 'melbourne') || (dtw.geo && dtw.geo.coordinates && dtw.geo.coordinates[0] > -38.903 && dtw.geo.coordinates[0] < -37.555 && dtw.geo.coordinates[1] > 144.26 && dtw.geo.coordinates[1] < 145.5686)) {\n            emit(\"melbourne\", 1);\n        }\n        else if ((dtw.place && dtw.place.name.toLowerCase() === 'sydney') || (dtw.geo && doc.geo.coordinates && dtw.geo.coordinates[0] > -34.1779 && dtw.geo.coordinates[0] < -33.493 && dtw.geo.coordinates[1] > 150.643 && dtw.geo.coordinates[1] < 151.394)) {\n            emit(\"sydney\", 1);\n        }\n        else {\n            emit(\"other\", 1);\n        }\n    }\n}"
#     },
#     "hourofDay": {
#       "map": "function (doc) {\n    if (doc.tweet) {\n        emit(new Date(doc.tweet.created_at).getHours(), doc.tweet.full_text);\n    }\n}"
#     },
#     "sentimentPosNeg": {
#       "reduce": "_count",
#       "map": "function (doc) {\n    if (doc.sentiment_results) {\n        if (doc.sentiment_results < 0) {\n            emit('negative', 1);\n        }\n        else if (doc.sentiment_results > 0) {\n            emit('positive', 1);\n        }\n        else if (doc.sentiment_results == 0) {\n            emit('neutral', 1);\n        }\n    }\n}"
#     },
#     "sentimentTenth": {
#       "reduce": "_count",
#       "map": "function (doc) {\n    if (doc.sentiment_results) {\n        emit(Number(doc.sentiment_results.toFixed(1)), 1);\n    }\n}"
#     }
#   },
#   "language": "javascript"
# }


def main():
    target_links = [countCity_link, countHashtag_link, hourofDay_link, sentimentTenth_link, sentimentPosNeg_link]
    links = [server_link + melb_view_link + l for l in target_links]
    result_lst = list_results(links)
    for r in result_lst:
        print(r)

main()