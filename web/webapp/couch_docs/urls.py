from django.urls import path

from . import views

app_name = 'couch_docs'
urlpatterns = [
    path('', views.index, name='index'),
]
