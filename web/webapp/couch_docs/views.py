from django.shortcuts import render
from django.http import Http404,HttpResponseRedirect
from couchdb import Server
from heapq import nlargest
from operator import itemgetter

# Create your views here.
server = None
with open('couch_docs/_couchdb_server_ip.config') as f:
    server = Server(f.readline())

def index(request):
    results = []
    view_results = server['melb_tweets'].view('newView/countHashtag', group=True)
    for data in view_results:
        results.append((data.key, data.value))

    results = nlargest(5, results, key=itemgetter(1))

    return render(request, 'couch_docs/index.html', {'results': results})
