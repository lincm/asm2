from django.apps import AppConfig


class CouchDocsConfig(AppConfig):
    name = 'couch_docs'
