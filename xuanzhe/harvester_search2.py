#!/usr/bin/env python
# -*- coding:utf-8 -*-
# @Time  : 2020/5/7 16:47
# @Author: xuanzhe
# @File  : harvester_search2.py


import time
import tweepy
import logging

class TwitterSearcher2():
    """Use Twitter search APIs find tweets from specific location."""

    def __init__(self, api, db, geo, query):
        """Set variables required by Twitter Search API."""
        self.api = api
        self.db = db
        self.geo = geo
        self.query = query


    def search(self):

        maxTweets = 10000  # Some arbitrary large number
        fName = 'C:\\Users\\78231\\Desktop\\test.txt'  # We'll store the tweets in a text file.

        # sinceId = None

        # If results only below a specific ID are, set max_id to that ID.
        # else default to no upper limit, start from the most recent tweet matching the search query.
        # max_id = -1
        tweet_count = 0

        def limit_handled(cursor):
            while True:
                try:
                    yield cursor.next()
                except tweepy.RateLimitError:
                    time.sleep(15 * 60)

        for new_tweets in limit_handled(tweepy.Cursor(self.api.search, geocode = self.geo, lang = 'en').items()):
            json_tweet = new_tweets._json
            id = json_tweet["id_str"]    # get id of each tweet
            if id in self.db:
                logging.info(id + "has already in database.")
            else:
                text = json_tweet['text']
                coordinates = json_tweet['coordinates']
                user = json_tweet['user']
                creat_time = json_tweet['created_at']
                place = json_tweet['place']
                entities = json_tweet['entities']
                doc = {'_id': id, 'text': text, 'user': user,
                        'coordinates': coordinates, 'create_time': creat_time,
                        'place': place, 'entities': entities}
                self.db.save(doc)
                print(place)
            tweet_count += 1
            if tweet_count % 100 == 0:
                logging.info("Downloading max {0} tweets".format(tweet_count))
            # print(type(json_tweet))
            # print(json_tweet["id"])
            # njson = json.dumps(new_tweets._json, ensure_ascii=False)
            #
            # tweet_count += 1
            # if tweet_count <= maxTweets:
            #     f = open(fName, 'a', encoding='utf-8')
            #     f.write(njson)
            #     f.write('\n')
            #     f.close()
            #     if tweet_count%100 == 0 :
            #         logging.info("Downloading max {0} tweets".format(tweet_count))


