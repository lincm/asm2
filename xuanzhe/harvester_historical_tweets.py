#!/usr/bin/env python
# -*- coding:utf-8 -*-
# @Time  : 2020/5/16 12:37
# @Author: xuanzhe
# @File  : harvester_historical_tweets.py
import json
import logging
import time

import couchdb
import tweepy
from textblob import TextBlob


class TwitterHistoricalSearch():

    def __init__(self, api, user_db, mined_db, location):
        self.api = api
        self.user_db = user_db
        self.mined_db = mined_db
        self.tweet_total = 0    # this parameter stands how many tweets we count in total
        self.max_tweets = 200
        self.location = location

    def mine_tweets(self, doc):
        last_tweet_id = False
        tweet_count = 0
        id_str = doc['_id']

        while tweet_count < self.max_tweets:
            try:
                if last_tweet_id:
                    statuses = self.api.user_timeline(user_id=id_str,
                                                      max_id=last_tweet_id - 1,
                                                      count=self.max_tweets,
                                                      tweet_mode='extended',
                                                      include_retweets=True
                                                      )
                    # count = self.result_limit,

                else:
                    statuses = self.api.user_timeline(user_id=id_str,
                                                      count=self.max_tweets,
                                                      tweet_mode='extended',
                                                      include_retweets=True)

                for item in statuses:
                    json_tweet = item._json
                    tweet_id = json_tweet["id_str"]    # get id of each tweet
                    json_text = json_tweet["full_text"]    # here is full_text because of tweet_mode parameter

                    try:
                        tweet_count += 1
                        # save non duplicated twitter
                        if tweet_id in self.mined_db:
                            logging.info(tweet_id + " has already in tweets database.")
                        else:
                            if json_tweet['place'] is None:
                                print('%s has no place' % str(tweet_id))
                            else:
                                tweet_place = json_tweet['place']['name'].lower()  # where the tweet is approximately from
                                if tweet_place == self.location:
                                    # to do analysis
                                    testimonial = TextBlob(json_text)
                                    polarity = testimonial.sentiment.polarity  # The polarity score is a float within the range [-1.0, 1.0]
                                    tweet_doc = {'_id': tweet_id, 'tweet': json_tweet, 'sentiment_results': polarity}

                                    self.mined_db.save(tweet_doc)
                                    self.tweet_total += 1

                                    if self.tweet_total % 100 == 0:
                                        logging.info("Downloading max {0} tweets".format(self.tweet_total))

                    except couchdb.http.ResourceConflict:
                        logging.info("Ignored duplicate tweet.")
                    except BaseException as e:
                        print("Error on_data: %s" % str(e))

                    # print(item)
                    # print(type(item.place))
                    # mined = {
                    #     '_id': str(item.id),
                    #     'name': item.user.name,
                    #     'screen_name': item.user.screen_name,
                    #     'retweet_count': item.retweet_count,
                    #     'text': item.full_text,
                    #     'hashtags': item.entities['hashtags'],
                    #     'mined_at': datetime.datetime.now().strftime( '%Y-%m-%d %H:%M:%S'),
                    #     'created_at': item.created_at.strftime( '%Y-%m-%d %H:%M:%S'),
                    #     'favourite_count': item.favorite_count,
                    #     'status_count': item.user.statuses_count,
                    #     'geo': item.geo,
                    #     'coordinates': item.coordinates,
                    #     'place': item.place,    # <class 'tweepy.models.Place'>    Object of type Place is not JSON serializable
                    #     'source_device': item.source,
                    #     'lang': item.lang
                    # }
                    # # print(datetime.now(), type(datetime.now()))
                    # try:
                    #     mined['retweet_text'] = item.retweeted_status.full_text
                    # except:
                    #     mined['retweet_text'] = 'None'
                    # try:
                    #     mined['quote_text'] = item.quoted_status.full_text
                    #     mined['quote_screen_name'] = item.quoted_status.user.screen_name
                    # except:
                    #     mined['quote_text'] = 'None'
                    #     mined['quote_screen_name'] = 'None'
                    #
                    # last_tweet_id = item.id
                    # self.mined_db.save(mined)

            except tweepy.RateLimitError:
                time.sleep(15 * 60)
                continue
            except tweepy.TweepError as e:
                print(id_str)
                logging.error("Error on user account: %s" % str(e))
                break
            except BaseException as e:
                print("Error in mine_tweets: %s" % str(e))

        doc["mined_status"] = True
        self.user_db.save(doc)


    def mine_user_tweets(self):

        try:
            # get id from user database
            for row_doc in self.user_db.view("_all_docs", include_docs = True):
                # doc in user_db is <class 'couchdb.client.Row'> type
                # print(row_doc)
                # print(type(row_doc))
                try:
                    user_doc = row_doc['doc']
                    # print(user_doc['id'])
                    if not user_doc['mined_status']:
                        self.mine_tweets(user_doc)

                except tweepy.TweepError as e:
                    print("Error TweepError Failed to run the command on that user, Skipping...: %s" % str(e))
                    continue
        except BaseException as e:
            print("Error on_id: %s" % str(e))
