#!/usr/bin/env python
# -*- coding:utf-8 -*-
# @Time  : 2020/4/30 14:59
# @Author: xuanzhe
# @File  : harvester.py
import argparse
import json
import logging
import sys
import tweepy
import couchdb

from http.client import IncompleteRead as http_incompleteRead
from urllib3.exceptions import IncompleteRead as urllib3_incompleteRead

from harvester_search1 import TwitterSearcher1
from harvester_stream import TwitterStreamListener
from harvester_historical_tweets import TwitterHistoricalSearch
# from harvester_search2 import TwitterSearcher2
# from harvester_screen_name import ScreenNameHarvester

def get_credentials(config):
    """Read and return credentials from config file."""
    with open(config) as fp:
        jconfig = json.load(fp)

        # Attempt to read authentification details from config file.
        try:
            c_key = jconfig['Authentication']['ConsumerKey']
            c_secret = jconfig['Authentication']['ConsumerSecret']
            a_token = jconfig['Authentication']['AccessToken']
            a_secret = jconfig['Authentication']['AccessTokenSecret']

        except Exception as e:
            logging.error(str(e))
            sys.exit(2)

        return c_key, c_secret, a_token, a_secret


def get_box(config):
    """Return a box representing locations defined in config file."""
    with open(config) as fp:
        jconfig = json.load(fp)

        try:
            box = [
                float(jconfig["location_info"]['location_box'][0]),
                float(jconfig["location_info"]['location_box'][1]),
                float(jconfig["location_info"]['location_box'][2]),
                float(jconfig["location_info"]['location_box'][3])
                ]
        except Exception as e:
            logging.error(str(e))
            print('get location box unsuccessfully')
            sys.exit(3)
    return box


def get_geocode(config):
    """Return geocode defined in config file."""
    with open(config) as fp:
        jconfig = json.load(fp)

        try:
            geocode = jconfig['location_info']["Geocode"]
            logging.info(geocode)
        except Exception as e:
            logging.error(str(e))
            print('get geocode unsuccessfully')
            sys.exit(4)

    return geocode


def get_db(config, ip):
    with open(config) as fp:

        try:
            jconfig = json.load(fp)

            admin = jconfig['db_info']['admin']
            password = jconfig['db_info']['password']

            '''  '''
            twitter_db = jconfig['location_info']['tweets_db']
            user_db = jconfig['location_info']['user_db']

            server = couchdb.Server('http://' + admin + ':' + password + '@'+ip+':5984/')    # 127.0.0.1 for local

            if twitter_db in server:
                print
                'Using existing database ' + twitter_db + ' from server'
                twitter_db = server[twitter_db]
            else:
                print
                'Creating database ' + twitter_db + ' on server'
                twitter_db = server.create(twitter_db)

            if user_db in server:
                print
                'Using existing database ' + user_db + ' from server'
                user_db = server[user_db]
            else:
                print
                'Creating database ' + user_db + ' on server'
                user_db = server.create(user_db)

        except Exception as e:
            logging.error(str(e))
            print('get database unsuccessfully')
            sys.exit(5)

        return twitter_db, user_db


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)

    parser = argparse.ArgumentParser(description='Test for argparse')
    parser.add_argument('--config', '-c', help='configure 属性，必要参数,有默认值', required=True)
    parser.add_argument('--server', '-s', help='server ip 属性，必要参数', required=True)
    parser.add_argument('--mode', '-m', help='server ip 属性，非必要参数，有默认值', default='stream')
    parser.add_argument('--keyword', '-k', help='melbourne or sydney', default='sydney')
    args = parser.parse_args()

    config = args.config
    ip = args.server
    mode = args.mode 
    key = args.keyword
    # 'test' for test mode, 'stream' for get specific data
    # config = 'test.json'
    # ip = '127.0.0.1'
    # mode = 'stream'
    tweets_db, user_db = get_db(config, ip)

# http://docs.tweepy.org/en/latest/getting_started.html#api
    c_key, c_secret, a_token, a_secret = get_credentials(config)

    if mode == 'stream':
        auth = tweepy.OAuthHandler(c_key, c_secret)
        auth.set_access_token(a_token, a_secret)
        api = tweepy.API(auth)

        box = get_box(config)
        stream_listener = TwitterStreamListener(tweets_db, user_db)
        api = tweepy.API(auth)
        stream = tweepy.Stream(auth = api.auth, listener = stream_listener)
        while True:
            try:
                stream.filter(locations=box)
            except http_incompleteRead as e:
                print("http.client Incomplete Read error: %s" % str(e))
                continue
            except urllib3_incompleteRead as e:
                print("urllib3 Incomplete Read error: %s" % str(e))
                continue
        """ find last exception solutions on https://github.com/tweepy/tweepy/issues/908 """

    elif mode == 'search':
        auth = tweepy.AppAuthHandler(c_key, c_secret)
        geo = get_geocode(config)
        api = tweepy.API(
            auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True
        )
        searcher = TwitterSearcher1(api, user_db, geo, "*")
        searcher.search()

    elif mode == 'timelines':
        auth = tweepy.AppAuthHandler(c_key, c_secret)
        api = tweepy.API(
            auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True
        )
        searcher = TwitterHistoricalSearch(api, user_db, tweets_db)
        searcher.mine_user_tweets()
    elif mode == 'prunDB':
        prunner = Prunner(tweets_db, key)
        prunner.prun()
    # if mode == 'search2':
    #     auth = tweepy.AppAuthHandler(c_key, c_secret)
    #     geo = get_geocode(config)
    #     api = tweepy.API(
    #         auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True
    #     )
    #     searcher = TwitterSearcher2(api, db, geo, "*")
    #     searcher.search()
    #
    # if mode == "screen_name":
    #     auth = tweepy.AppAuthHandler(c_key, c_secret)
    #     geo = get_geocode(config)
    #     api = tweepy.API(
    #         auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True
    #     )
    #     searcher = ScreenNameHarvester(api, db, geo, "*")
    #     searcher.search()

class Prunner:
    def __init__(self, db, city):
        self.city = city
        self.db = db
        self.docs = [] 
    
    def prun(self):
        try:
            for docid in self.db.view("_all_docs", include_docs = True):
                try:
                    user_doc = docid['doc']
                    if user_doc['tweet']['place'] == None or user_doc['tweet']['place']['name']!=self.city:
                        user_doc['_deleted'] = True
                        self.docs.append(user_doc)
                        
                except tweepy.TweepError as e:
                    print("Error TweepError Failed to run the command on that user, Skipping...: %s" % str(e))
                    continue

            self.db.update(self.docs)       

        except BaseException as e:
            print("Error on_id: %s" % str(e))

