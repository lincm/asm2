#!/usr/bin/env python
# -*- coding:utf-8 -*-
# @Time  : 2020/5/7 16:07
# @Author: xuanzhe
# @File  : harvester_stream.py

import logging
import json
import couchdb

from textblob import TextBlob
from tweepy.streaming import StreamListener


class TwitterStreamListener(StreamListener):
    """Listen using Twitter Streaming API."""

    def __init__(self, tweets_db, user_db, location):
        """Store reference to couchdb."""
        self.tweets_db = tweets_db
        self.user_db = user_db
        self.location = location
        self.tweet_count = 0
        self.user_count = 0

    def on_data(self, data):
        """Store tweet, if not already seen."""
        json_tweet = json.loads(data)
        tweet_id = json_tweet["id_str"]    # get id of each tweet
        json_text = json_tweet["text"]    # get text from tweet preparing to analysis
        user_id = str(json_tweet["user"]["id"])

        try:
            # save non duplicated twitter
            if tweet_id in self.tweets_db:
                logging.info(tweet_id + " has already in tweets database.")
            else:
                if json_tweet['place'] is None:
                    print('%s has no place' % str(tweet_id))
                else:
                    tweet_place = json_tweet['place']['name'].lower()  # where the tweet is approximately from
                    if tweet_place == self.location:
                        testimonial = TextBlob(json_text)
                        polarity = testimonial.sentiment.polarity    # The polarity score is a float within the range [-1.0, 1.0]
                        tweet_doc = {'_id': tweet_id, 'tweet': json_tweet, 'sentiment_results': polarity}

                        self.tweets_db.save(tweet_doc)
                        self.tweet_count += 1

                        if self.tweet_count % 100 == 0:
                            logging.info("Downloading max {0} tweets".format(self.tweet_count))

            # get user's information from each tweets
            if user_id in self.user_db:
                logging.info(user_id + " has already in user database.")
            else:
                id = json_tweet["user"]["id"]
                name = json_tweet["user"]["name"]
                screen_name = json_tweet["user"]["screen_name"]
                location = json_tweet["user"]["location"]
                mined = False
                user_doc = {'_id': user_id, 'id': id, 'name': name, 'screen_name': screen_name, 'location': location, "mined_status": mined}
                self.user_db.save(user_doc)
                self.user_count += 1

                if self.user_count % 100 == 0:
                    logging.info("Downloading max {0} user information".format(self.user_count))

        except couchdb.http.ResourceConflict:
            logging.info("Ignored duplicate tweet.")
        except BaseException as e:
            print("Error on_data: %s" % str(e))

    def on_error(self, status_code):
        """Log error message."""
        logging.error(status_code)