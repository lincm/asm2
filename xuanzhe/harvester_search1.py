"""
Use Twitter search APIs find tweets from specific location.
Raw tweets are stored in specified couchDB databse.
"""

import logging
import time

import tweepy
from textblob import TextBlob


class TwitterSearcher1():
    """Use Twitter search APIs find tweets from specific location."""

    def __init__(self, api, db, geo, query):
        """Set variables required by Twitter Search API."""
        self.api = api
        self.db = db
        self.geo = geo
        self.query = query

        # API rate call limit.
        self.limit = 100

    def search(self):
        """Search for tweets via Twitter Search API."""
        # Track the upper and lower bound of each returned set.
        lower_id = None
        upper_id = -1
        print(self.geo)
        # Track number of tweets returned in total.
        self.tweet_count = 0

        # Pull tweets until erorr or no more to process.
        while True:
            try:
                if (upper_id <= 0):
                    if (not lower_id):
                        new_tweets = self.api.search(
                            q=self.query,
                            geocode=self.geo,
                            count=self.limit
                        )

                    else:
                        new_tweets = self.api.search(
                            q=self.query,
                            geocode=self.geo,
                            count=self.limit,
                            since_id=lower_id
                        )
                else:
                    if (not lower_id):
                        new_tweets = self.api.search(
                            q=self.query,
                            geocode=self.geo,
                            count=self.limit,
                            upper_id=str(upper_id - 1)
                        )
                    else:
                        new_tweets = self.api.search(
                            q=self.query,
                            geocode=self.geo,
                            count=self.limit,
                            upper_id=str(upper_id - 1),
                            since_id=lower_id
                        )

                # Exit when no new tweets are found.
                if not new_tweets:
                    logging.info("No more tweets found.")
                    break

                # Process received tweets.
                for tweet in new_tweets:

                    json_tweet = tweet._json
                    id = json_tweet["id_str"]  # get id of each tweet
                    json_text = json_tweet["text"]  # get text from tweet preparing to analysis
                    if id in self.db:
                        logging.info(id + " has already in database.")
                    else:
                        testimonial = TextBlob(json_text)
                        polarity = testimonial.sentiment.polarity  # The polarity score is a float within the range [-1.0, 1.0]
                        doc = {'_id': id, 'tweet': json_tweet, 'sentiment_results': polarity}
                        # print(polarity)
                        self.db.save(doc)
                        self.tweet_count += 1

                        if self.tweet_count % 100 == 0:
                            logging.info("Downloading max {0} tweets".format(self.tweet_count))

                # Track upper id.
                upper_id = new_tweets[-1].id

            except tweepy.RateLimitError:
                time.sleep(15 * 60)
            # Exit upon error.
            except tweepy.TweepError as e:
                logging.error(str(e))
                break
